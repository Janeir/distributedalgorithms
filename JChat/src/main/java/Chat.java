import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Chat extends ReceiverAdapter {
    protected JChannel channel;

    public void viewAccepted(View new_view) {
        System.out.println("** view: " + new_view);
    }

    public void receive(Message msg) {
        String payload = msg.getObject();
        System.out.printf("[%s]: %s\n", msg.getSrc(), payload);
    }


    private void start(String config, String name) throws Exception {
        channel = new JChannel(config).name(name).receiver(this);
        channel.connect("Chat");
        readingKey();
        channel.close();
    }

    private void readingKey() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                System.out.print("> ");
                System.out.flush();
                String line = in.readLine().toLowerCase();
                if (line.startsWith("quit") || line.startsWith("exit"))
                    break;
                Message msg = new Message(null, line);
                channel.send(msg);
            } catch (Exception e) {
            }
        }
    }


    public static void main(String[] args) throws Exception {
        String config = "config.xml";
        String name = "TestName";
        new Chat().start(config, name);
    }
}